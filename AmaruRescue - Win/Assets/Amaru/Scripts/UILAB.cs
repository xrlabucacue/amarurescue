using UnityEngine;

public class UILAB : MonoBehaviour
{
     private bool activado = false;
    private float tiempoDesactivacion = 5f;

    private void OnEnable()
    {
        if (!activado)
        {
            activado = true;
            Invoke("DesactivarObjeto", tiempoDesactivacion);
        }
    }

    private void OnDisable()
    {
        // Cancelar el temporizador si el objeto se desactiva antes de los 5 segundos
        CancelInvoke("DesactivarObjeto");
        activado = false;
    }

    private void DesactivarObjeto()
    {
        gameObject.SetActive(false);
    }
}