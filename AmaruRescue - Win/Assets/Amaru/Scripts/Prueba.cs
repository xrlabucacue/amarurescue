using System.Collections;
using UnityEngine;
using TMPro;

public class Prueba : MonoBehaviour
{
    public TextMeshProUGUI preguntaTexto; // Texto donde mostrar la pregunta
    public TextMeshProUGUI respuestaATexto; // Texto donde mostrar la respuesta A
    public TextMeshProUGUI respuestaBText; // Texto donde mostrar la respuesta B

    private int preguntaActual = 0; // Índice de la pregunta actual
    private int puntaje = 0; // Puntaje del jugador
    private bool respondiendo = false; // Indica si el jugador está respondiendo

    // Lista de preguntas y respuestas
    private PreguntaRespuesta[] preguntas = new PreguntaRespuesta[]
    {
        new PreguntaRespuesta("¿Cuál es el nombre científico de una de las 2 rana la cual esta en peligro de extinción?", "Atelopus exiguus", "Hyloscirtus tapichalaca", "b"),
        new PreguntaRespuesta("¿Qué característica distingue a Hyloscirtus tapichalaca?", "Color exotico", "Ventosas en las patas", "b"),
        new PreguntaRespuesta("¿Dónde se encuentra Hyloscirtus tapichalaca?", "En los desiertos de Australia", "En los bosques nublados de alta montaña en Ecuador", "b"),
        new PreguntaRespuesta("¿Cuál es el hábitat de Hyloscirtus tapichalaca?", "Bosques tropicales húmedos", "Bosques nublados de alta montaña", "b"),
        new PreguntaRespuesta("¿Qué tipo de temperatura prefiere Hyloscirtus tapichalaca?", "10 grados Centígrados", "22-25 grados Centígrados", "b"),
        new PreguntaRespuesta("¿Qué porcentaje de humedad relativa requiere Hyloscirtus tapichalaca?", "50%", "70-80%", "b"),
        new PreguntaRespuesta("¿De qué se alimenta Hyloscirtus tapichalaca?", "Hojas, Flores", "Polillas, insectos pequeños, mariposas y orugas", "b"),
        new PreguntaRespuesta("¿Cuál es el nombre científico del 'Jambato de Mazán'?", "Atelopus exiguus", "Hyloscirtus tapichalaca", "a"),
        new PreguntaRespuesta("¿Dónde habita el 'Jambato de Mazán'?", "En el Amazonas", "En la vegetación de páramo del parque nacional Cajas", "b"),
        new PreguntaRespuesta("¿Qué tipo de vegetación se encuentra en el hábitat del 'Jambato de Mazán'?", "Selva tropical", "Vegetación de páramo", "b"),
    };

    // Clase para representar una pregunta con sus respuestas y la respuesta correcta
    private class PreguntaRespuesta
    {
        public string pregunta;
        public string respuestaA;
        public string respuestaB;
        public string respuestaCorrecta;

        public PreguntaRespuesta(string pregunta, string respuestaA, string respuestaB, string respuestaCorrecta)
        {
            this.pregunta = pregunta;
            this.respuestaA = respuestaA;
            this.respuestaB = respuestaB;
            this.respuestaCorrecta = respuestaCorrecta;
        }
    }

    private void Start()
    {
        MostrarSiguientePregunta();
    }

    private void Update()
    {
        if (respondiendo)
        {
            if (OVRInput.GetDown(OVRInput.Button.One))
            {
                SeleccionarRespuesta("a");
            }
            else if (OVRInput.GetDown(OVRInput.Button.Two))
            {
                SeleccionarRespuesta("b");
            }
        }
    }

    private void MostrarSiguientePregunta()
    {
        if (preguntaActual < preguntas.Length)
        {
            preguntaTexto.text = preguntas[preguntaActual].pregunta;
            respuestaATexto.text = "A) " + preguntas[preguntaActual].respuestaA;
            respuestaBText.text = "B) " + preguntas[preguntaActual].respuestaB;
            respondiendo = true;
        }
        else
        {
            // El jugador ha respondido todas las preguntas
            FinalizarJuego();
        }
    }

    private void SeleccionarRespuesta(string respuesta)
    {
        if (respuesta == preguntas[preguntaActual].respuestaCorrecta)
        {
            puntaje++;
        }
        preguntaActual++;
        respondiendo = false;
        StartCoroutine(EsperarYMostrarSiguientePregunta());
    }

    private IEnumerator EsperarYMostrarSiguientePregunta()
    {
        yield return new WaitForSeconds(2f); // Esperar 2 segundos antes de mostrar la siguiente pregunta
        MostrarSiguientePregunta();
    }

    private void FinalizarJuego()
    {
        preguntaTexto.text = "Puntaje Final: " + puntaje + " / " + preguntas.Length;
        respuestaATexto.text = "";
        respuestaBText.text = "";
    }
}
