using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiempoEspera : MonoBehaviour

{
    public float tiempoDeEspera = 5f; // Tiempo en segundos para esperar antes de activar el objeto
    public GameObject objetoUI; // Objeto con tag "UI" que se activará

    private void Start()
    {
        // Deshabilitar el objeto con tag "UI" al inicio (asegúrate de que esté deshabilitado en el Inspector)
        if (objetoUI != null)
        {
            objetoUI.SetActive(false);
        }

        // Iniciar la corrutina para activar el objeto después del tiempo de espera
        StartCoroutine(ActivarObjetoDespuesDeTiempoCorrutina());
    }

    private IEnumerator ActivarObjetoDespuesDeTiempoCorrutina()
    {
        // Esperar el tiempo especificado antes de activar el objeto
        yield return new WaitForSeconds(tiempoDeEspera);

        // Activar el objeto con tag "UI"
        if (objetoUI != null)
        {
            objetoUI.SetActive(true);
        }
    }
}
