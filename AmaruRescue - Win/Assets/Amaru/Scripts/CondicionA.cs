using UnityEngine;
using System.Collections;

public class CondicionA : MonoBehaviour
{
    public bool useCoordinates = false; // Variable para determinar si se usarán coordenadas o un objeto como destino
    public Transform targetRanaA; // Si useCoordinates es false, se usará este objeto como destino
    public Vector3 teleportPosition; // Coordenadas a las que se teletransportará el objeto con tag "RANA A"
    public GameObject uiRanaA; // Objeto con tag "UI RANA A" que se activará
    public GameObject uiCOMA; // Objeto con tag "UI COM A" que se activará
    public GameObject uiError; // Objeto con tag "UI ERROR" que se activará

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("RANA A"))
        {
            // Activar el componente con tag "UI RANA A"
             if (uiRanaA != null)
              {
              uiRanaA.SetActive(true);
              }
            // Iniciar la corutina para teletransportar después de 2 segundos
            
            StartCoroutine(TeleportAfterDelay(other.gameObject));
        }
        else if (other.CompareTag("COM A"))
        {
            // Activar el componente con tag "UI COM A"
            if (uiCOMA != null)
            {
                uiCOMA.SetActive(true);
            }
        }
        else if (!other.CompareTag("Player")) // Si no es "RANA A" ni "COM A" ni "Player"
        {
            // Activar el componente con tag "UI ERROR"
            if (uiError != null)
            {
                uiError.SetActive(true);
            }
        }
    }

    private IEnumerator TeleportAfterDelay(GameObject ranaObject)
    {
        // Esperar 2 segundos
        yield return new WaitForSeconds(2f);

        // Teletransportar el objeto con tag "RANA A" según las coordenadas o el objeto destino seleccionado
        if (useCoordinates)
        {
            ranaObject.transform.position = teleportPosition;
        }
        else
        {
            ranaObject.transform.position = targetRanaA.position;
        }

        
    }
}
