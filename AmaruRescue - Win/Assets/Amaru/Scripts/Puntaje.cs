using UnityEngine;

public class Puntaje : MonoBehaviour
{
    [SerializeField] private GameObject objeto1; // Objeto a activar si el tiempo es mayor a 2 minutos
    [SerializeField] private GameObject objeto2; // Objeto a activar si el tiempo es mayor a 1 minuto pero menor a 2 minutos
    [SerializeField] private GameObject objeto3; // Objeto a activar si el tiempo es menor a 1 minuto

    private float tiempoTotal = 180f; // Tiempo total en segundos (3 minutos)
    private float tiempoTranscurrido = 0f;
    private bool tiempoFinalizado = false;

    private void Start()
    {
        tiempoTranscurrido = 0f;
    }

    private void Update()
    {
        if (!tiempoFinalizado)
        {
            tiempoTranscurrido += Time.deltaTime;
            if (tiempoTranscurrido >= tiempoTotal)
            {
                tiempoFinalizado = true;
                EvaluarTiempo();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("rana"))
        {
            tiempoFinalizado = true;
            EvaluarTiempo();
        }
    }

    private void EvaluarTiempo()
    {
        if (tiempoTranscurrido < 60f)
        {
            ActivarObjeto(objeto3);
        }
        else if (tiempoTranscurrido < 120f)
        {
            ActivarObjeto(objeto2);
        }
        else
        {
            ActivarObjeto(objeto1);
        }
    }

    private void ActivarObjeto(GameObject objeto)
    {
        if (objeto != null)
        {
            objeto.SetActive(true);
        }
    }
}
