using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CargarEscena : MonoBehaviour
{
    public string sceneName; // Nombre de la escena que se cargará después de 3 segundos

    private void Start()
    {
        // Iniciar la corrutina para cargar la escena después de 3 segundos
        StartCoroutine(LoadSceneWithDelay());
    }

    private IEnumerator LoadSceneWithDelay()
    {
        // Esperar 3 segundos
        yield return new WaitForSeconds(3f);

        // Cargar la escena especificada por el nombre
        SceneManager.LoadScene(sceneName);
    }
}
