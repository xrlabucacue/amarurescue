using UnityEngine;
public class PropagacionFuego : MonoBehaviour
{
    public GameObject fuegoPrefab; // Prefab del fuego
    public int cantidadPrefabs = 10; // Cantidad de prefabs a multiplicar
    public float distanciaInicial = 1f; // Distancia inicial desde el centro
    public float distanciaIncremento = 1f; // Incremento de distancia entre los prefabs
    public float tiempoPropagacion = 1f; // Tiempo de propagación entre cada prefab
    private float tiempoActual = 0f;
    private int prefabsCreados = 0;
    private Terrain terrain;
    private Vector3 posicionInicial;
    private TerrainCollider terrainCollider;
    private void Start()
    {
        terrain = Terrain.activeTerrain;
        terrainCollider = terrain.GetComponent<TerrainCollider>();
        posicionInicial = transform.position;

        StartCoroutine(PropagarFuego());
    }

    private System.Collections.IEnumerator PropagarFuego()
    {
        while (prefabsCreados < cantidadPrefabs)
        {
            float t = tiempoActual / tiempoPropagacion;
            float factorInterpolacion = t;

            float distancia = distanciaInicial + prefabsCreados * distanciaIncremento;
            Vector3 posicionExpandida = posicionInicial + Random.insideUnitSphere * distancia;
            float alturaTerreno = terrain.SampleHeight(posicionExpandida);
            Vector3 posicionTerreno = new Vector3(posicionExpandida.x, alturaTerreno, posicionExpandida.z);

            if (alturaTerreno > 2.5f && terrainCollider.bounds.Contains(posicionTerreno))
            {
                RaycastHit hit;
                if (Physics.Raycast(posicionTerreno + Vector3.up * 10f, Vector3.down, out hit, Mathf.Infinity, LayerMask.GetMask("Terrain")))
                {
                    posicionTerreno = hit.point;
                }

                GameObject fuego = Instantiate(fuegoPrefab, posicionTerreno, Quaternion.identity);
                fuego.transform.SetParent(transform);
                prefabsCreados++;
            }

            tiempoActual += Time.deltaTime;

            yield return new WaitForSeconds(tiempoPropagacion);
        }
    }
}
