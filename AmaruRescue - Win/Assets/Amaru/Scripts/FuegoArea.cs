using UnityEngine;
public class FuegoArea : MonoBehaviour
{
    public GameObject fuegoPrefab; // Prefab del fuego
    public int cantidadPrefabs = 10; // Cantidad de prefabs a multiplicar
    public float distanciaInicial = 1f; // Distancia inicial desde el centro
    public float distanciaArea = 5f; // Distancia del área de propagación
    public float alturaPrefab = 1f; // Altura del prefab generado sobre el terreno
    public float tiempoPropagacion = 1f; // Tiempo de propagación entre cada prefab
    private float tiempoActual = 0f;
    private int prefabsCreados = 0;
    private Terrain terrain;
    private Vector3 posicionInicial;
    private TerrainCollider terrainCollider;
    private void Start()
    {
        terrain = Terrain.activeTerrain;
        terrainCollider = terrain.GetComponent<TerrainCollider>();
        posicionInicial = transform.position;
        StartCoroutine(PropagarFuego());
    }
    private System.Collections.IEnumerator PropagarFuego()
    {
        while (prefabsCreados < cantidadPrefabs)
        {
            float t = tiempoActual / tiempoPropagacion;
            float factorInterpolacion = t;
            float distancia = distanciaInicial + prefabsCreados * 1f;
            Vector2 posicionExpandida2D = new Vector2(posicionInicial.x, posicionInicial.z) + Random.insideUnitCircle.normalized * distancia;
            Vector3 posicionExpandida = new Vector3(posicionExpandida2D.x, 0f, posicionExpandida2D.y);
            float alturaTerreno = terrain.SampleHeight(new Vector3(posicionExpandida.x, 0f, posicionExpandida.z));
            Vector3 posicionTerreno = new Vector3(posicionExpandida.x, alturaTerreno, posicionExpandida.z);
            float distanciaDesdeCentro = Vector3.Distance(posicionInicial, posicionTerreno);
            if (distanciaDesdeCentro <= distanciaArea && alturaTerreno >= 2.5f && alturaTerreno <= 4f && terrainCollider.bounds.Contains(posicionTerreno))
            {
                RaycastHit hit;
                if (Physics.Raycast(posicionTerreno + Vector3.up * 10f, Vector3.down, out hit, Mathf.Infinity, LayerMask.GetMask("Terrain")))
                {
                    posicionTerreno = hit.point;
                }

                Vector3 posicionPrefab = new Vector3(posicionTerreno.x, alturaTerreno + alturaPrefab, posicionTerreno.z);
                GameObject fuego = Instantiate(fuegoPrefab, posicionPrefab, Quaternion.identity);
                fuego.transform.SetParent(transform);
                prefabsCreados++;
            }

            tiempoActual += Time.deltaTime;
            yield return new WaitForSeconds(tiempoPropagacion);
        }
    }
}
