using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class PulsaX2 : MonoBehaviour
{
    public List<string> nombresDeEscenas = new List<string> { "Construccion", "Construccion 1", "Construccion 2" };

    void Update()
    {
        // Verificar si el jugador ha presionado la tecla X (OVRInput.Button.Three)
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            // Seleccionar una escena aleatoria de la lista de nombresDeEscenas
            int indiceAleatorio = Random.Range(0, nombresDeEscenas.Count);
            string nombreDeEscenaAleatoria = nombresDeEscenas[indiceAleatorio];

            // Cambiar a la escena seleccionada aleatoriamente
            SceneManager.LoadScene(nombreDeEscenaAleatoria);
        }
    }
}
