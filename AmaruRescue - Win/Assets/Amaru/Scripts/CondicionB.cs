using UnityEngine;
using System.Collections;
public class CondicionB : MonoBehaviour
{
    public bool useCoordinates = false; // Variable para determinar si se usarán coordenadas o un objeto como destino
    public Transform targetRanaB; // Si useCoordinates es false, se usará este objeto como destino
    public Vector3 teleportPosition; // Coordenadas a las que se teletransportará el objeto con tag "RANA A"
    public GameObject uiRanaB; // Objeto con tag "UI RANA A" que se activará
    public GameObject uiCOMB; // Objeto con tag "UI COM A" que se activará
    public GameObject uiError; // Objeto con tag "UI ERROR" que se activará

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("RANA B"))
        {
            

            // Activar el componente con tag "UI RANA B"
            if (uiRanaB != null)
            {
                uiRanaB.SetActive(true);
            }
             // Iniciar la corutina para teletransportar después de 2 segundos
            StartCoroutine(TeleportAfterDelay(other.gameObject));
        }
        else if (other.CompareTag("COM B"))
        {
            // Activar el componente con tag "UI COM B"
            if (uiCOMB != null)
            {
                uiCOMB.SetActive(true);
            }
        }
        else if (!other.CompareTag("Player")) // Si no es "RANA B" ni "COM B" ni "Player"
        {
            // Activar el componente con tag "UI ERROR"
            if (uiError != null)
            {
                uiError.SetActive(true);
            }
        }
    }
    private IEnumerator TeleportAfterDelay(GameObject ranaObject)
    {
        // Esperar 2 segundos
        yield return new WaitForSeconds(2f);

        // Teletransportar el objeto con tag "RANA B" según las coordenadas o el objeto destino seleccionado
        if (useCoordinates)
        {
            ranaObject.transform.position = teleportPosition;
        }
        else
        {
            ranaObject.transform.position = targetRanaB.position;
        }

        
    }
}
