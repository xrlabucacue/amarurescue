using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnorarColision : MonoBehaviour

{
    private void Start()
    {
        int ranaLayer = LayerMask.NameToLayer("RANA");
        int playerLayer = LayerMask.NameToLayer("PLAYER");

        // Ignorar la colisión entre los layers "Rana" y "Player"
        Physics.IgnoreLayerCollision(ranaLayer, playerLayer);
    }
}
