using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    public string sceneNameToLoad; // Nombre de la escena que se cargará después de la colisión con el objeto con tag "Player"

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Cargar la nueva escena
            SceneManager.LoadScene(sceneNameToLoad);
        }
    }
}

