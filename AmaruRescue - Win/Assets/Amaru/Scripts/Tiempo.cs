using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Tiempo : MonoBehaviour
{
    public string sceneName = "MainMenu";
    public float tiempoTotal = 180f; // 3 minutos en segundos
    private float tiempoRestante;
    private int minutos;
    private int segundos;
    public TextMeshProUGUI tiempoText;

    private void Start()
    {
        tiempoRestante = tiempoTotal;
        ActualizarTextoTiempo();
    }

    private void Update()
    {
        if (tiempoRestante > 0f)
        {
            tiempoRestante -= Time.deltaTime;
            ActualizarTextoTiempo();
        }
        else
        {
            // Aquí puedes poner lo que desees cuando el tiempo se acabe
            Debug.Log("¡Tiempo agotado!");
            SceneManager.LoadScene(sceneName);
        }
    }

    public void ActualizarTextoTiempo()
    {
        // Calcular los minutos y segundos restantes
        minutos = Mathf.FloorToInt(tiempoRestante / 60f);
        segundos = Mathf.FloorToInt(tiempoRestante % 60f);

        // Formatear el tiempo restante en minutos y segundos
        string tiempoFormateado = string.Format("{0:00}:{1:00}", minutos, segundos);

        // Asignar el tiempo formateado al componente TextMeshProUGUI
        tiempoText.text = tiempoFormateado;
    }
}
