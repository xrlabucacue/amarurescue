using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoHerida : MonoBehaviour
{
    public AudioClip sonido; // Sonido que se reproducirá cuando ocurra la colisión

    private void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto con el que colisionó tiene el tag "Fire"
        if (collision.gameObject.CompareTag("Fire"))
        {
            // Obtener el componente AudioSource del objeto de sonido
            AudioSource audioSource = GetComponent<AudioSource>();

            // Verificar si se encontró un componente AudioSource
            if (audioSource == null)
            {
                // Agregar un componente AudioSource si no existe
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            // Asignar el clip de sonido al AudioSource
            audioSource.clip = sonido;

            // Reproducir el sonido
            audioSource.Play();
        }
    }
}