using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActDesControlesMap : MonoBehaviour

{
    public GameObject objeto1;
    public GameObject objeto2;
    private bool objetosActivos = true;

    void Update()
    {
        // Reemplaza "joystick button 2" por el nombre del botón X en el input de Oculus.
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            objetosActivos = !objetosActivos;
            objeto1.SetActive(objetosActivos);
            objeto2.SetActive(objetosActivos);
        }
    }
}
