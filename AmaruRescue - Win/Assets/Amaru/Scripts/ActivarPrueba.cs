using UnityEngine;

public class ActivarPrueba : MonoBehaviour
{
    public GameObject objeto3D; // El objeto 3D que deseas activar.

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Activa el objeto 3D.
            if (objeto3D != null)
            {
                objeto3D.SetActive(true);
            }

            // Desactiva este objeto si es necesario.
            gameObject.SetActive(false);
        }
    }
}
