using UnityEngine;
using UnityEngine.SceneManagement;

public class PulsaX : MonoBehaviour
{
    public string nombreDeEscena = "Lab";

    void Update()
    {
        // Verificar si el jugador ha presionado la tecla X (OVRInput.Button.Three)
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            // Cambiar a la escena especificada por el nombreDeEscena
            SceneManager.LoadScene(nombreDeEscena);
        }
    }
}
