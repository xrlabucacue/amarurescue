using UnityEngine;

public class MarcaStart : MonoBehaviour
{
    public string playerTag = "Player";
    public GameObject[] fuegoPrefabs; // Array de prefabs de fuego
    public string uiTag = "UI";

    // Agrega una matriz de objetos hijos para asignar desde el inspector
    public GameObject[] objetosHijo;

    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag))
        {
            // Activar los objetos de fuego
            foreach (GameObject fuegoPrefab in fuegoPrefabs)
            {
                GameObject fuego = Instantiate(fuegoPrefab, fuegoPrefab.transform.position, fuegoPrefab.transform.rotation);
                fuego.SetActive(true);
            }

            // Apagar los objetos con etiqueta "UI"
            GameObject[] objetosUI = GameObject.FindGameObjectsWithTag(uiTag);
            foreach (GameObject obj in objetosUI)
            {
                // Desactivar el componente Canvas si está presente
                Canvas canvas = obj.GetComponent<Canvas>();
                if (canvas != null)
                {
                    canvas.enabled = false;
                }
                else
                {
                    // Si no tiene componente Canvas, desactivar el objeto completo
                    obj.SetActive(false);
                }
            }

            // Activar los objetos hijos (si se han asignado desde el inspector)
            foreach (GameObject hijo in objetosHijo)
            {
                if (hijo != null)
                {
                    hijo.SetActive(true);
                }
            }

            
        }
    }
}
