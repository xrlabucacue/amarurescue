using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonMenu : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Cambiar a la escena "MainMenu"
            SceneManager.LoadScene("MenuPrincipalNew");
        }
    }
}
