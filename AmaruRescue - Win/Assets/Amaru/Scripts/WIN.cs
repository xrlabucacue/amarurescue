using UnityEngine;
using UnityEngine.SceneManagement;

public class WIN : MonoBehaviour
{
    public string sceneName = "Win";
    public float delayBeforeSceneChange = 5f; // Tiempo de espera antes de cambiar de escena

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("rana"))
        {
            // Desactivar el objeto con el tag "rana"
            other.gameObject.SetActive(false);

            // Llamar a la función para cambiar de escena después del tiempo de espera
            Invoke("ChangeSceneWithDelay", delayBeforeSceneChange);
        }
    }

    private void ChangeSceneWithDelay()
    {
        // Cambiar a la escena especificada después del tiempo de espera
        SceneManager.LoadScene(sceneName);
    }
}


