using UnityEngine;
using System.Collections;

public class RanaControllerAtelopus : MonoBehaviour
{
    public float velocidadMovimiento = 0.5f;
    public float tiempoEsperaMin = 0f;
    public float tiempoEsperaMax = 0f;
    public float velocidadAnimacion = 2f;
    public AnimationClip nadarAnimation; // Animacion de nadar
    public AnimationClip animacionQuieta; // Animacion cuando la rana esta quieta
    public float distanciaDesplazamiento = 6f; // Distancia de desplazamiento maximo
    public Transform playerTransform; // Transform del OVRPlayerController
    public float distanciaUmbral = 2f; // Distancia umbral para empezar a moverse
    private Vector3 movimientoDireccion;
    private Animator animator;
    private CharacterController characterController;
    private bool isPlayerNearby = false; // Variable para rastrear si el jugador está cerca
    private bool isMoving = false; // Variable para rastrear si la rana está en movimiento

    void Start()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
        StartCoroutine(MoverAleatoriamente());

        // Reproducir animación inicial de estar quieta
        animator.Play(animacionQuieta.name);
    }

    void Update()
    {
        // Verificar si el jugador está cerca
        float distancia = Vector3.Distance(transform.position, playerTransform.position);
        isPlayerNearby = distancia <= distanciaUmbral;

        // Verificar si la rana está en el agua (posición Y <= 1)
        bool isInWater = transform.position.y <= 0f;
        bool isInHand = transform.position.y >= 9f;

        if ((isPlayerNearby || isInWater) && !isInHand)
        {
            // Aplicar movimiento horizontal y vertical
            characterController.Move(movimientoDireccion * velocidadMovimiento * Time.deltaTime);

            // La rana está en movimiento, no reproducir la animación de estar quieta
            isMoving = true;
        }
        else
        {
            // La rana no está en movimiento o está siendo agarrada, reproducir animación cuando la rana está quieta
            if (!isMoving)
            {
                animator.Play(animacionQuieta.name);
            }
        }

        // Mantener la rana al nivel del suelo
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
        {
            float alturaSuelo = hit.point.y;
            transform.position = new Vector3(transform.position.x, Mathf.Max(alturaSuelo, 0f), transform.position.z);

            // Comprobar si la rana esta en el agua o es agarrada y cambiar la animacion
            if (isInWater)
            {
                animator.Play(nadarAnimation.name);
            }
            else
            {
                animator.SetBool("IsSwimming", false);
            }

            if (isInHand)
            {
                animator.Play(animacionQuieta.name);
                isMoving = false;
            }
        }

        // Ajustar la velocidad de las animaciones
        animator.speed = velocidadAnimacion;

        // Reiniciar el estado de movimiento a falso
        isMoving = false;
    }

    IEnumerator MoverAleatoriamente()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(tiempoEsperaMin, tiempoEsperaMax));

            // Generar un vector de dirección aleatorio
            Vector3 direccionAleatoria = Random.insideUnitCircle.normalized;
            movimientoDireccion = new Vector3(direccionAleatoria.x, 0f, direccionAleatoria.y);

            // Limitar la distancia de desplazamiento
            movimientoDireccion *= distanciaDesplazamiento;

            // Calcular la nueva posición
            Vector3 nuevaPosicion = transform.position + movimientoDireccion;

            if (movimientoDireccion != Vector3.zero)
            {
                Quaternion toRotation = Quaternion.LookRotation(movimientoDireccion, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, 180f);
            }

            animator.SetBool("IsJumping", false);
            animator.SetBool("IsCrawling", false);

            if (Random.value < 0.8f)
            {
                animator.SetBool("IsJumping", true);
            }
            else if (Random.value < 0.60f)
            {
                animator.SetBool("IsCrawling", true);
            }
            else if (Random.value < 0.4f)
            {
                animator.SetFloat("TurnDirection", -1f);
                animator.SetTrigger("LeftTurn");
            }
            else
            {
                animator.SetFloat("TurnDirection", 1f);
                animator.SetTrigger("RightTurn");
            }
        }
    }
}
